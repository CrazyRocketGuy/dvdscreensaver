﻿using Microsoft.Win32;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace DVDScreensaver
{
    public partial class ScreenSaverForm : Form
    {
        [DllImport("user32.dll")]
        private static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern bool GetClientRect(IntPtr hWnd, out Rectangle lpRect);

        private bool previewMode = false;

        private Random rand = new Random();

        private int velX;
        private int velY;

        private int screenX;
        private int screenY;

        private int imgColor = 1;

        public ScreenSaverForm(Rectangle Bounds)
        {
            //Initialized via Normal Means
            InitializeComponent();
            this.Bounds = Bounds;
            screenX = Bounds.Width;
            screenY = Bounds.Height;
        }

        public ScreenSaverForm(IntPtr PreviewWndHandle)
        {
            //Initialized via Preview
            InitializeComponent();

            // Set the preview window as the parent of this window
            SetParent(this.Handle, PreviewWndHandle);

            // Make this a child window so it will close when the parent dialog closes
            // GWL_STYLE = -16, WS_CHILD = 0x40000000
            SetWindowLong(this.Handle, -16, new IntPtr(GetWindowLong(this.Handle, -16) | 0x40000000));

            // Place our window inside the parent
            Rectangle ParentRect;
            GetClientRect(PreviewWndHandle, out ParentRect);
            Size = ParentRect.Size;
            Location = new Point(0, 0);

            screenX = Size.Width;
            screenY = Size.Height;

            previewMode = true;
        }

        private void ScreenSaverForm_Load(object sender, System.EventArgs e)
        {
            //Pull Transparency Settings
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\DVDScreensaver");
            if (key == null) { 
            
            }

            else
            {

            }
                
                        
                        //(string)key.GetValue("Transparency");


            if (previewMode)
            {
                velX = 1;
                velY = 1;

                dvdImage.Width = 50;
                dvdImage.Height = 25;
            }
            else
            {
                                
                //Velocity based on screen size
                velX = Convert.ToInt32(Math.Round((rand.Next(2, 3) / 1920f) * screenX));
                velY = Convert.ToInt32(Math.Round((rand.Next(2, 3) / 1080f) * screenY));

                //Random Start Direction
                if (rand.Next(0, 2) == 1)
                {
                    velX *= -1;
                }
                if (rand.Next(0, 2) == 1)
                {
                    velY *= -1;
                }
                //Random Start Location

                dvdImage.Left = rand.Next(0, screenX - dvdImage.Width);
                dvdImage.Top = rand.Next(0, screenY - dvdImage.Height);

                //Scale Logo Size with screen size

                dvdImage.Width = Convert.ToInt32(Math.Round((270 / 1920f) * screenX));
                dvdImage.Height = dvdImage.Width / 2;

                //Scale Logo Speed with screen size

                //Start Timer
            }
            Cursor.Hide();
            TopMost = true;

            dvdImage.Image = DVDScreensaver.Properties.Resources.dvdRed;
            imgColor = rand.Next(0, 2);
            updateDraw();

            moveTimer.Interval = 1000 / 100;
            moveTimer.Tick += new EventHandler(moveTimer_Tick);
            moveTimer.Start();
        }

        private void moveTimer_Tick(object sender, System.EventArgs e)
        {
            if (dvdImage.Location.X < 0)
            {
                velX = velX * -1;
                dvdImage.Left = 0;
                updateDraw();
            }

            if (dvdImage.Left + dvdImage.Width > screenX)
            {
                velX = velX * -1;
                dvdImage.Left = screenX - dvdImage.Width;
                updateDraw();
            }
            if (dvdImage.Top < 0)
            {
                velY = velY * -1;
                dvdImage.Top = 0;
                updateDraw();
            }

            if (dvdImage.Top + dvdImage.Height > screenY)
            {
                velY = velY * -1;
                dvdImage.Top = screenY - dvdImage.Height;
                updateDraw();
            }
            // Move text to new location
            dvdImage.Left = dvdImage.Left + velX;
            dvdImage.Top = dvdImage.Top + velY;
        }

        private void updateDraw()
        {
            //Update Image Color
            if (imgColor == 0)
            {
                dvdImage.Image = DVDScreensaver.Properties.Resources.dvdRed;
                imgColor++;
            }
            else if (imgColor == 1)
            {
                dvdImage.Image = DVDScreensaver.Properties.Resources.dvdBlue;
                imgColor++;
            }
            else if (imgColor == 2)
            {
                dvdImage.Image = DVDScreensaver.Properties.Resources.dvdGreen;
                imgColor++;
            }
            else if (imgColor == 3)
            {
                dvdImage.Image = DVDScreensaver.Properties.Resources.dvdWhite;
                imgColor++;
            }
            if (imgColor > 3)
            {
                imgColor = 0;
            }
        }

        //Kill the Screensaver if there is a Click, Press, or Move
        private void ScreenSaverForm_MouseClick(object sender, MouseEventArgs e)
        {
            if (!previewMode)
                Application.Exit();
        }

        private Point mouseLocation;

        private void ScreenSaverForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (!previewMode)
            {
                if (!mouseLocation.IsEmpty)
                {
                    // Terminate if mouse is moved a significant distance
                    if (Math.Abs(mouseLocation.X - e.X) > 5 ||
                        Math.Abs(mouseLocation.Y - e.Y) > 5)
                        Application.Exit();
                }

                // Update current mouse location
                mouseLocation = e.Location;
            }
        }

        private void ScreenSaverForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!previewMode)
                Application.Exit();
        }
    }
}